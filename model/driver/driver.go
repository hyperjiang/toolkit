package driver

import "time"

// Driver -
type Driver struct {
	DriverID   string    `gorm:"column:driver_id;primaryKey"`
	Name       string    `gorm:"column:full_name"`
	Phone      string    `gorm:"column:phone_no"`
	CreatedAt  time.Time `gorm:"column:txCreate"`
	ModifiedAt time.Time `gorm:"column:txModify"`
}

// TableName overrides the table name
func (Driver) TableName() string {
	return "vandriver"
}
