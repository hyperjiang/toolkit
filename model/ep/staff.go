package ep

import "time"

// Staff -
type Staff struct {
	ID          uint      `gorm:"column:staff_id;primaryKey"`
	CorporateID string    `gorm:"column:ep_id"`
	Name        string    `gorm:"column:name_cn"`
	Phone       string    `gorm:"column:phone_no"`
	Role        uint      `gorm:"column:role"`
	CreatedAt   time.Time `gorm:"column:create_time"`
	ModifiedAt  time.Time `gorm:"column:modify_time"`
}

// TableName overrides the table name
func (Staff) TableName() string {
	return "t_staff"
}
