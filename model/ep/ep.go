package ep

import "time"

// Ep -
type Ep struct {
	ID          uint
	CorporateID string    `gorm:"column:ep_id"`
	Name        string    `gorm:"column:name"`
	CreatedAt   time.Time `gorm:"column:create_time"`
	ModifiedAt  time.Time `gorm:"column:modify_time"`
}

// TableName overrides the table name
func (Ep) TableName() string {
	return "t_ep"
}
