package user

import "time"

// Order -
type Order struct {
	OrderID           string     `gorm:"column:order_id"`
	DisplayID         uint64     `gorm:"column:order_display_id"`
	UUID              string     `gorm:"column:order_uuid"`
	City              uint       `gorm:"column:city_id"`
	OrderStatus       uint       `gorm:"column:order_status"`       // 0: Untaken; 1:On going; 2: Completed; 3: Canceled; 4: Rejected
	OrderFleetStatus  uint       `gorm:"column:order_fleet_status"` // 0: Normal; 1: Fleet first; 2: Fleet ended; 3: Fleet only
	OrderGroup        int        `gorm:"column:order_group"`
	CorporateID       uint       `gorm:"column:ep_id"`
	UserID            string     `gorm:"column:user_id"`
	Tel               string     `gorm:"column:tel"`
	DriverID          string     `gorm:"column:driver_id"`
	CouponID          uint       `gorm:"column:coupon_id"`
	CouponPrice       int        `gorm:"column:coupon_price_fen"`
	BasePrice         uint       `gorm:"column:base_price_fen"`
	TotalPrice        uint       `gorm:"column:total_price_fen"`
	PayPrice          uint       `gorm:"column:pay_price_fen"`
	TipsPrice         uint       `gorm:"column:tips_price_fen"`
	OpAddPrice        uint       `gorm:"column:op_add_price_fen"`     // 平台补贴金额
	PerquisitePrice   uint       `gorm:"column:perquisite_price_fen"` // 后置支付中的额外费用
	WaitingFee        uint       `gorm:"column:waiting_fee_fen"`
	ExceedDistance    int        `gorm:"column:exceed_distance"`
	Remark            string     `gorm:"column:remark"`
	ClientType        uint       `gorm:"column:client_type"` // 0-未知 1-Android 2-IOS 3-webapp 4-wechat
	VehicleID         uint       `gorm:"column:order_vehicle_id" json:"-"`
	PhysicsVehicleIDs string     `gorm:"column:physics_vehicle_ids" json:"-"`
	OrderDateTime     time.Time  `gorm:"column:order_datetime"`
	ReroutedAt        *time.Time `gorm:"column:reroute_time"`
	CanceledAt        *time.Time `gorm:"column:cancel_time"`
	CompletedAt       *time.Time `gorm:"column:complete_time"`
	CreatedAt         time.Time  `gorm:"column:create_time"`
	ModifiedAt        time.Time  `gorm:"column:modify_time"`
	CashReceivedAt    time.Time  `gorm:"column:cash_received_time"` // 司机点击已收现金按钮的时间
	PaidAt            time.Time  `gorm:"column:pay_time"`
	PayType           uint       `gorm:"column:pay_type"`                 // 前置支付支付方式（0:现金,1:微信支付,2:支付宝支付,4:纯钱包支付,31:收银台）
	PayStatus         uint       `gorm:"column:pay_status"`               // 支付状态：(0:待支付, 1:已支付，2:支付失败，3:退款中，4:已退款)
	RearPayStatus     uint       `gorm:"column:rear_pay_status" json:"-"` // 0:未支付, 1:已支付, 5:支付中
	RearPayItemIDs    string     `gorm:"column:rear_pay_item_ids" json:"-"`
	Invoice           uint       `gorm:"column:invoice_fen"`
	Brokerage         uint       `gorm:"column:brokerage_fen"`
	PaidAmount        uint       `gorm:"column:paid_amount_fen"`
	DriverAmount      uint       `gorm:"column:driver_amount_fen"` // 司机入账金额
}

// TableName overrides the table name
func (Order) TableName() string {
	return "t_order"
}
