package user

import "time"

// Payment -
type Payment struct {
	ID           uint      `gorm:"column:idvanpayment"`
	OrderNO      string    `gorm:"column:txUUID"`
	PayType      uint      `gorm:"column:pay_type"` // 支付方式（0:现金,1:微信支付,2:支付宝支付,4:纯钱包支付）
	TotalFee     int       `gorm:"column:total_fee"`
	CashFee      int       `gorm:"column:cash_fee"`
	CouponFee    int       `gorm:"column:coupon_fee"`
	TradeStatus  uint      `gorm:"column:trade_status"` // 交易状态 1：成功，2：失败
	Transaction  string    `gorm:"column:transaction"`  // 交易号
	RefundStatus uint      `gorm:"column:refund"`       // 是否退款，0:未退款，1:申请中，2：退款中（已发请求），3：已退款（退款成功）
	PaidAt       time.Time `gorm:"column:txPay"`
	RefundedAt   time.Time `gorm:"column:txrefund"`
	CreatedAt    time.Time `gorm:"column:txCreate"`
	ModifiedAt   time.Time `gorm:"column:txModify"`
	Usefor       uint      `gorm:"column:usefor"` // 1：下单在线支付，2：保证金，3：vip套餐，4：后置支付
}

// TableName overrides the table name
func (Payment) TableName() string {
	return "vanpayment"
}
