package user

import "time"

// Fleet -
type Fleet struct {
	ID          uint
	UserID      uint      `gorm:"column:idvansmslogin"`
	DriverID    uint      `gorm:"column:servicer_idvanDriver"`
	CorporateID uint      `gorm:"column:ep_id"`
	IsFavorite  bool      `gorm:"column:is_favorite"`
	IsBanned    bool      `gorm:"column:is_ban"`
	CreatedAt   time.Time `gorm:"column:txCreate"`
	ModifiedAt  time.Time `gorm:"column:txModify"`
}

// TableName overrides the table name
func (Fleet) TableName() string {
	return "vanmyfleet"
}
