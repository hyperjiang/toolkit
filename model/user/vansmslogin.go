package user

import "time"

// Vansmslogin -
type Vansmslogin struct {
	UserID     string    `gorm:"column:user_id;primaryKey"`
	NickName   string    `gorm:"column:nickname"`
	RealName   string    `gorm:"column:real_name"`
	Phone      string    `gorm:"column:phone_no"`
	CreatedAt  time.Time `gorm:"column:txCreate"`
	ModifiedAt time.Time `gorm:"column:txModify"`
}

// TableName overrides the table name
func (Vansmslogin) TableName() string {
	return "vansmslogin"
}
