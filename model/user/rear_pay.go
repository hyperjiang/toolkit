package user

import "time"

// RearPay -
type RearPay struct {
	OrderID        uint      `gorm:"column:order_id"`
	SerialNO       string    `gorm:"column:serial_no"`
	Fare           int       `gorm:"column:rear_pay_fare_fen"`   // 后置支付包含的用车费，在线支付的订单，后置支付的时候这里是0
	Tips           int       `gorm:"column:tips_fen"`            // 小费
	PerquisiteFee  int       `gorm:"column:perquisite_fee_fen"`  // 额外费用
	CombinedWallet int       `gorm:"column:combined_wallet_fen"` // 组合支付中钱包支付的金额
	PayType        uint      `gorm:"column:pay_type"`            // 前置支付支付方式（0:现金,1:微信支付,2:支付宝支付,4:纯钱包支付）
	PayStatus      uint      `gorm:"column:pay_status"`          // 支付状态; 0:未支付, 1:已支付, 5:支付中
	PaidAt         time.Time `gorm:"column:pay_time"`
	CreatedAt      time.Time `gorm:"column:create_time"`
	ModifiedAt     time.Time `gorm:"column:modify_ts"`
}

// TableName overrides the table name
func (RearPay) TableName() string {
	return "t_rear_pay"
}
