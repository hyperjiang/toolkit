package bill

import (
	"time"

	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/util"
)

// DwalletEntry - 司机入账表
type DwalletEntry struct {
	ID          uint64    `gorm:"column:entry_id;primaryKey"`
	OrderID     string    `gorm:"column:order_id"`
	DriverID    string    `gorm:"column:servicer_id"`
	BillType    uint      `gorm:"column:bill_type"`
	Amount      int       `gorm:"column:entry_amount_fen"`
	Status      uint      `gorm:"column:entry_status"` // 入账状态：1待用户支付 2待入账 3已入账 4已扣款 5入账失败 6现金无需入账 7订单取消，不入账 8账单取消
	EntryTime   time.Time `gorm:"column:entry_time"`
	CreatedAt   time.Time `gorm:"column:create_time"`
	ModifiedAt  time.Time `gorm:"column:modify_time"`
	City        uint      `gorm:"column:city_id"`
	Transaction string    `gorm:"column:entry_transaction"` // 钱包入账交易号，唯一标识
	Currency    string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m DwalletEntry) TableName() string {
	if viper.GetString("env") == "stg" {
		return "t_dwallet_entry_00" + util.Mod(m.OrderID, 2)
	}

	return "t_dwallet_entry_" + util.Mod(m.OrderID, 256)
}
