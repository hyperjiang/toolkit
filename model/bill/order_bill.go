package bill

import (
	"time"

	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/util"
)

// OrderBill - 订单账单表
type OrderBill struct {
	ID           int       `gorm:"column:bill_id;primaryKey"`
	OrderID      string    `gorm:"column:order_id"`
	UserID       string    `gorm:"column:user_id"`
	CorporateID  uint      `gorm:"column:ep_id"`
	BillType     uint      `gorm:"column:bill_type"`
	Price        int       `gorm:"column:price_fen"`
	PaidPrice    int       `gorm:"column:paid_price_fen"`
	RefundAmount int       `gorm:"column:refund_amount_fen"`
	PayType      uint      `gorm:"column:pay_type"`   // 支付方式: 0现金支付 1微信支付 2支付宝支付 3企业钱包 4纯钱包支付 31收银台
	OrderType    uint      `gorm:"column:order_type"` // 订单类型: 1小B 2企业 3搬家
	PayStatus    uint      `gorm:"column:pay_status"` // 支付状态: 101待支付 102已支付 103已取消 104无需支付 105账单取消 201退款中 202已退款 203退款失败
	PaidAt       time.Time `gorm:"column:paid_time"`
	CreatedAt    time.Time `gorm:"column:create_time"`
	ModifiedAt   time.Time `gorm:"column:modify_time"`
	City         uint      `gorm:"column:city_id"`
	Currency     string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m OrderBill) TableName() string {
	if viper.GetString("env") == "stg" {
		return "t_order_bill_00" + util.Mod(m.OrderID, 2)
	}

	return "t_order_bill_" + util.Mod(m.OrderID, 256)
}
