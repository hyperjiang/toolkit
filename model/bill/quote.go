package bill

import (
	"time"

	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/util"
)

// Quote - 支付场景表
type Quote struct {
	ID           int       `gorm:"column:quote_id;primaryKey"`
	TradeNO      string    `gorm:"column:trade_no"` // 交易单号=db_hpay.t_gw_pay_flow_xx.b_order_no
	OrderID      string    `gorm:"column:order_id"`
	Transaction  string    `gorm:"column:transaction"` // 支付系统交易流水号=db_hpay.t_gw_pay_flow_xx.c_trade_no
	PayType      uint      `gorm:"column:pay_type"`    // 0现金支付 1微信支付 2支付宝支付 3企业钱包 4纯钱包支付 31收银台
	Amount       int       `gorm:"column:amount_fen"`
	PaidAmount   int       `gorm:"column:paid_amount_fen"`
	UsedCoupon   uint      `gorm:"column:used_coupon"`
	Status       uint      `gorm:"column:trans_status"` // 交易状态 1等待支付 2支付成功 3支付失败 4重复支付
	PayChannelID uint      `gorm:"column:pay_channel_id"`
	PaidAt       time.Time `gorm:"column:paid_time"`
	CreatedAt    time.Time `gorm:"column:create_time"`
	ModifiedAt   time.Time `gorm:"column:modify_time"`
	City         uint      `gorm:"column:city_id"`
	Currency     string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m Quote) TableName() string {
	if viper.GetString("env") == "stg" {
		return "t_quote_00" + util.Mod(m.OrderID, 2)
	}

	return "t_quote_" + util.Mod(m.OrderID, 256)
}
