package bill

import "time"

// RefundFlow - 退款流水表
type RefundFlow struct {
	ID              int       `gorm:"column:refund_id;primaryKey"`
	RefundNO        string    `gorm:"column:refund_no"`
	OrderID         string    `gorm:"column:order_id"`
	UserID          string    `gorm:"column:user_id"`
	BillIDs         string    `gorm:"column:bill_ids"`
	PaymentIDs      string    `gorm:"column:payment_ids"`
	TradeNO         string    `gorm:"column:trade_no"`
	Transaction     string    `gorm:"column:transaction"`
	RefundStatus    uint      `gorm:"column:refund_status"` // 1申请退款 2退款中 3成功 4失败
	PaymentType     uint      `gorm:"column:payment_type"`
	RefundAmount    int       `gorm:"column:refund_amount_fen"`
	RefundChannel   uint      `gorm:"column:refund_channel"`
	RefundType      uint      `gorm:"column:refund_type"`       // 1即时退款 2延时退款
	BeginRefundTime time.Time `gorm:"column:begin_refund_time"` // 开始退款时间(会加上延迟时间)
	RefundedAt      time.Time `gorm:"column:refund_time"`
	VerifyStatus    uint      `gorm:"column:verify_status"` // 延迟退款核实状态，0 未核实 1 已核实
	OriginalAmount  int       `gorm:"column:refund_original_fen"`
	PayChannelID    uint      `gorm:"column:pay_channel_id"`
	PunishmentType  string    `gorm:"column:punishment_type"`
	CreatedAt       time.Time `gorm:"column:create_time"`
	ModifiedAt      time.Time `gorm:"column:modify_time"`
	City            uint      `gorm:"column:city_id"`
}

// TableName overrides the table name
func (RefundFlow) TableName() string {
	return "t_refund_flow"
}
