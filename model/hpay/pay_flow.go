package hpay

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// PayFlow - 支付流水表
type PayFlow struct {
	ID            uint
	AppID         uint      `gorm:"column:app_id"`
	UID           uint      `gorm:"column:uid"`
	Uflag         uint      `gorm:"column:uflag"` // 用户类型:1-用户,2-司机,3-企业
	OrderNO       string    `gorm:"column:b_order_no"`
	Amount        uint      `gorm:"column:amount_fen"`
	PayNO         string    `gorm:"column:p_pay_no"`
	PayChannelID  uint      `gorm:"column:pay_channel_id"`
	PayCnfID      uint      `gorm:"column:pay_cnf_id"`
	TradeNO       string    `gorm:"column:c_trade_no"` // 第三方交易号
	RetCode       string    `gorm:"column:c_ret_code"` // 支付渠道返回交易状态码
	RetMsg        string    `gorm:"column:c_ret_msg"`  // 支付渠道返回交易信息
	PayerID       string    `gorm:"column:c_payer_id"`
	PayerAccount  string    `gorm:"column:c_payer_account"`
	PayerBankName string    `gorm:"column:c_payer_bank_name"`
	PaySubType    uint      `gorm:"column:c_pay_sub_type"` // 付款卡类型:1-信用卡,2-储蓄卡,3-零钱
	PaidAt        uint      `gorm:"column:c_pay_its"`      // 支付渠道支付完成时间
	NotifyType    uint      `gorm:"column:c_notify_type"`  // 回调处理方式:1-异步,2-同步,3-主动查询
	NotifiedAt    uint      `gorm:"column:p_pay_its"`      // 收到回调通知的时间
	PayStatus     uint      `gorm:"column:p_pay_status"`   // 交易状态:1-待支付,2-支付完成
	RefundStatus  uint      `gorm:"column:exist_refund"`
	CreatedAt     time.Time `gorm:"column:create_time"`
	ModifiedAt    time.Time `gorm:"column:modify_ts"`
	Country       uint      `gorm:"column:hcountry"`
	City          uint      `gorm:"column:city_id"`
	Currency      string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m PayFlow) TableName() string {
	return "t_gw_pay_flow_" + util.Circle(m.OrderNO, 1024, 32)
}
