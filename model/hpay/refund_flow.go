package hpay

import "time"

// RefundFlow - 支付渠道退款流水
type RefundFlow struct {
	ID            uint
	PRefundNO     string    `gorm:"column:p_refund_no"` // 支付系统退款单号
	PayNO         string    `gorm:"column:p_pay_no"`    // 原支付单号
	Amount        uint      `gorm:"column:amount_fen"`
	PayChannelID  uint      `gorm:"column:pay_channel_id"`
	PayCnfID      uint      `gorm:"column:pay_cnf_id"`
	TransRefundNO string    `gorm:"column:p_trans_refund_no"` // 网关退款单号
	CRefundNO     string    `gorm:"column:c_refund_no"`       // 支付渠道退款单号
	RetStatus     string    `gorm:"column:c_ret_status"`      // 支付渠道返回状态
	RetMsg        string    `gorm:"column:c_ret_msg"`         // 支付渠道返回信息
	RefundStatus  uint      `gorm:"column:p_refund_status"`   // 退款状态: 1-退款中, 2-退款成功, 3-申请失败, 4-退款失败
	RefundedAt    uint      `gorm:"column:c_refund_its"`      // 渠道侧退款时间
	CreatedAt     time.Time `gorm:"column:create_time"`
	ModifiedAt    time.Time `gorm:"column:modify_ts"`
	Country       uint      `gorm:"column:hcountry"`
	City          uint      `gorm:"column:city_id"`
	Currency      string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (RefundFlow) TableName() string {
	return "t_gw_refund_flow"
}
