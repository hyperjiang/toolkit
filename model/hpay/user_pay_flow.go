package hpay

import "time"

// UserPayFlow - 账户流水记录表
type UserPayFlow struct {
	ID           uint
	AppID        uint      `gorm:"column:app_id"`
	UID          uint      `gorm:"column:uid"`
	Uflag        uint      `gorm:"column:uflag"` // 用户类型:1-用户,2-司机,3-企业
	OrderNO      string    `gorm:"column:b_order_no"`
	Amount       uint      `gorm:"column:amount_fen"`
	PayNO        string    `gorm:"column:p_pay_no"`
	PayChannelID uint      `gorm:"column:pay_channel_id"`
	PayCnfID     uint      `gorm:"column:pay_cnf_id"`
	PayerID      string    `gorm:"column:c_payer_id"`
	TradeNO      string    `gorm:"column:c_trade_no"` // 支付渠道交易号
	CreatedAt    time.Time `gorm:"column:create_time"`
	Country      uint      `gorm:"column:hcountry"`
	City         uint      `gorm:"column:city_id"`
	Currency     string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m UserPayFlow) TableName() string {
	return "t_gw_user_pay_flow"
}
