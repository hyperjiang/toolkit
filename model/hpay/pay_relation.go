package hpay

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// PayRelation - 订单支付关系
type PayRelation struct {
	ID           uint
	AppID        string    `gorm:"column:app_id"`
	UID          string    `gorm:"column:uid"`
	OrderNO      string    `gorm:"column:b_order_no"` // 业务订单号
	PayNO        string    `gorm:"column:p_pay_no"`   // 支付订单号
	Amount       uint      `gorm:"column:amount_fen"`
	PayChannelID uint      `gorm:"column:pay_channel_id"`
	PayCnfID     uint      `gorm:"column:pay_cnf_id"`
	TradeNO      string    `gorm:"column:c_trade_no"`   // 第三方交易号
	OrderStatus  uint      `gorm:"column:order_status"` // 交易状态:1-待支付,2-支付完成,3-已关闭
	OrderID      string    `gorm:"column:order_id"`     // 业务侧对外展示的订单号
	CreatedAt    time.Time `gorm:"column:create_time"`
	ModifiedAt   time.Time `gorm:"column:modify_ts"`
	Country      string    `gorm:"column:hcountry"`
	City         string    `gorm:"column:city_id"`
	Currency     string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m PayRelation) TableName() string {
	return "t_gw_pay_relation_" + util.YearMonth(m.CreatedAt)
}
