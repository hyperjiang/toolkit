package hpay

import "time"

// RefundApply - 退款单记录表
type RefundApply struct {
	ID                 uint
	AppID              string    `gorm:"column:app_id"`
	UID                uint      `gorm:"column:uid"`
	Uflag              uint      `gorm:"column:uflag"` // 用户类型:1-用户,2-司机,3-企业
	OrderNO            string    `gorm:"column:b_order_no"`
	PayNO              string    `gorm:"column:p_pay_no"`              // 原支付单号
	TradeNO            string    `gorm:"column:c_trade_no"`            // 原支付渠道交易号
	RefundType         uint      `gorm:"column:refund_type"`           // 退款类型:1-正常退款,2-重复支付,3-金额不一致,4-逾期支付,5-钱包即时回退
	IsSys              uint      `gorm:"column:is_sys"`                // 0-正常退款,1-其他类型退款
	OrderRefundNO      string    `gorm:"column:b_order_refund_no"`     // 退款单号
	OriginalAmount     uint      `gorm:"column:orig_pay_amount_fen"`   // 原支付金额
	WalletPayAmount    uint      `gorm:"column:wallet_pay_amount_fen"` // 钱包支付金额
	TotalAmount        uint      `gorm:"column:total_amount_fen"`      // 退款总金额
	Amount             uint      `gorm:"column:c_amount_fen"`          // 支付渠道退款金额
	CombineAmount      uint      `gorm:"column:combine_amount_fen"`    // 组合退款钱包金额
	CombinePayWalletNO string    `gorm:"column:combine_pay_wallet_no"` // 组合支付钱包支付号
	PayChannelID       uint      `gorm:"column:pay_channel_id"`
	PayCnfID           uint      `gorm:"column:pay_cnf_id"`
	PRefundNO          string    `gorm:"column:p_refund_no"`     // 支付系统退款单号
	RefundStatus       uint      `gorm:"column:p_refund_status"` // 退款单状态:1-审核中,2-待打款,3-打款中,4-到渠道申请失败,5-渠道退款异常,6-已退款
	CreatedAt          time.Time `gorm:"column:create_time"`
	ModifiedAt         time.Time `gorm:"column:modify_ts"`
	RetryNum           uint      `gorm:"column:retry_fail_num"`
	Country            string    `gorm:"column:hcountry"`
	City               string    `gorm:"column:city_id"`
	Currency           string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (RefundApply) TableName() string {
	return "t_gw_refund_apply"
}
