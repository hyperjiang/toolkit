package hpay

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// PayApply - 支付网关表
type PayApply struct {
	ID           uint
	AppID        uint      `gorm:"column:app_id"`
	UID          uint      `gorm:"column:uid"`
	Uflag        uint      `gorm:"column:uflag"` // 用户类型:1-用户,2-司机,3-企业
	OrderNO      string    `gorm:"column:b_order_no"`
	Amount       uint      `gorm:"column:amount_fen"`
	GoodsName    string    `gorm:"column:goods_name"`
	GoodsDesc    string    `gorm:"column:goods_desc"`
	NotifyStatus uint      `gorm:"column:p_notify_status"` // 订单通知状态:1-未通知,2-通知成功
	CompletedAt  int       `gorm:"column:p_pay_its"`
	PayChannelID uint      `gorm:"column:pay_channel_id"`
	PayCnfID     uint      `gorm:"column:pay_cnf_id"`
	TradeNO      string    `gorm:"column:c_trade_no"`   // 第三方交易号
	OrderStatus  uint      `gorm:"column:order_status"` // 订单状态:1-未完成,2-支付完成,3-已关闭
	CreatedAt    time.Time `gorm:"column:create_time"`
	ModifiedAt   time.Time `gorm:"column:modify_ts"`
	ExpiredAt    time.Time `gorm:"column:expire_at_its"`
	Country      uint      `gorm:"column:hcountry"`
	City         uint      `gorm:"column:city_id"`
	Currency     string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m PayApply) TableName() string {
	return "t_gw_pay_apply_" + util.Circle(m.OrderNO, 1024, 16)
}
