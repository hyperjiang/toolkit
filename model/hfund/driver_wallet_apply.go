package hfund

import (
	"regexp"
	"time"

	jsoniter "github.com/json-iterator/go"
	"gitlab.com/hyperjiang/toolkit/util"
)

// DriverWalletApply -
type DriverWalletApply struct {
	ID          uint
	DriverID    string    `gorm:"column:driver_id"`
	ApplyNO     string    `gorm:"column:b_apply_no"`   // 业务申请单号,若无需上传则为钱包交易单号
	DwalletNO   string    `gorm:"column:p_dwallet_no"` // 钱包交易单号
	TradeType   uint      `gorm:"column:trade_type"`   // 交易类型:100-提现,200-订单完成,300-车贴广告费,400-购买会员费,500-保证金退还,600-手工增加,700-手工减少,800-车贴罚款索赔,900-虚报完成扣除,1000-活动奖励
	Amount      uint      `gorm:"column:amount_fen"`
	ExtraParams string    `gorm:"column:extra_params"`
	Status      uint      `gorm:"column:status"`    // 交易状态:1-待处理,2-处理中,3-已撤销,4-已入账
	BillTime    time.Time `gorm:"column:bill_time"` // 撤销或入账时间
	CreatedAt   time.Time `gorm:"column:create_time"`
	ModifiedAt  time.Time `gorm:"column:modify_time"`
	Country     uint      `gorm:"column:hcountry"`
	City        uint      `gorm:"column:city_id"`
	Currency    string    `gorm:"column:hcurrency"`
}
type ExtraParams struct {
	Remark string `json:"remark"`
	OpID   string `json:"op_id"`
	OpUser string `json:"op_user"`
	OpIP   string `json:"op_ip"`
}

// TableName overrides the table name
func (m DriverWalletApply) TableName() string {
	return "t_gw_driver_wallet_apply_" + util.Mod(m.DriverID, 32)
}

func (m DriverWalletApply) ParseExtraParams() ExtraParams {
	var params ExtraParams
	jsoniter.UnmarshalFromString(m.ExtraParams, &params)

	return params
}

func (p ExtraParams) ParseRemark() (string, string) {
	exp := `^(.+)/调整单号:(\d+)$`
	reg := regexp.MustCompile(exp)
	if m := reg.FindStringSubmatch(p.Remark); m != nil {
		return m[1], m[2]
	}

	return "", ""
}
