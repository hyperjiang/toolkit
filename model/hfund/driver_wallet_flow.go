package hfund

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// DriverWalletFlow - 司机钱包账户资金变动流水
type DriverWalletFlow struct {
	ID               uint
	DriverID         string    `gorm:"column:driver_id"`
	DwalletNO        string    `gorm:"column:p_dwallet_no"` // 钱包交易单号
	BillType         uint      `gorm:"column:bill_type"`    // 入账类型:1-待处理,2-撤销,3-完成
	TradeType        uint      `gorm:"column:trade_type"`
	TradeDetailType  uint      `gorm:"column:trade_detail_type"`
	ApplyNO          string    `gorm:"column:b_apply_no"` // 参考单号
	BeforeAvlBalance int       `gorm:"column:before_avl_balance_fen"`
	BeforeFrzBalance int       `gorm:"column:before_frz_balance_fen"`
	BeforeBalance    int       `gorm:"column:before_balance_fen"`
	DiffAvlBalance   int       `gorm:"column:diff_avl_balance_fen"`
	DiffFrzBalance   int       `gorm:"column:diff_frz_balance_fen"`
	DiffBalance      int       `gorm:"column:diff_balance_fen"`
	AfterAvlBalance  int       `gorm:"column:after_avl_balance_fen"`
	AfterFrzBalance  int       `gorm:"column:after_frz_balance_fen"`
	AfterBalance     int       `gorm:"column:after_balance_fen"`
	CreatedAt        time.Time `gorm:"column:create_time"`
	ModifiedAt       time.Time `gorm:"column:modify_time"`
	Country          uint      `gorm:"column:hcountry"`
	City             uint      `gorm:"column:city_id"`
	Currency         string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m DriverWalletFlow) TableName() string {
	return "t_gw_driver_wallet_flow_" + util.Mod(m.DriverID, 32)
}
