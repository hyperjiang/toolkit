package hfund

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// UserWalletFlow - 司机钱包账户资金变动流水
type UserWalletFlow struct {
	ID               uint
	UserID           string    `gorm:"column:uid"`
	WalletNO         string    `gorm:"column:p_wallet_no"` // 钱包交易单号
	FlowType         uint      `gorm:"column:flow_type"`   // 资金变动类型:10-支付宝充值,11-微信支付充值,20-支付订单,30-订单退款,41-系统调整增加,42-系统调整减少
	BeforeAvlBalance int       `gorm:"column:before_avl_balance_fen"`
	BeforeFrzBalance int       `gorm:"column:before_frz_balance_fen"`
	BeforeBalance    int       `gorm:"column:before_balance_fen"`
	AvlBalance       int       `gorm:"column:avl_balance_fen"`
	FrzBalance       int       `gorm:"column:frz_balance_fen"`
	Amount           int       `gorm:"column:amount_fen"`
	AfterAvlBalance  int       `gorm:"column:after_avl_balance_fen"`
	AfterFrzBalance  int       `gorm:"column:after_frz_balance_fen"`
	AfterBalance     int       `gorm:"column:after_balance_fen"`
	Remark           string    `gorm:"column:remark"`
	CreatedAt        time.Time `gorm:"column:create_time"`
	ModifiedAt       time.Time `gorm:"column:modify_time"`
	AppID            uint      `gorm:"column:app_id"`
	Country          uint      `gorm:"column:hcountry"`
	City             uint      `gorm:"column:city_id"`
	Currency         string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m UserWalletFlow) TableName() string {
	return "t_gw_user_wallet_flow_" + util.Mod(m.UserID, 32)
}
