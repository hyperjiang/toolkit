package hfund

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// UserWalletApply -
type UserWalletApply struct {
	ID         uint
	UserID     string    `gorm:"column:uid"`
	ApplyNO    string    `gorm:"column:b_apply_no"`  // 业务申请单号
	WalletNO   string    `gorm:"column:p_wallet_no"` // 钱包交易单号
	ApplyType  uint      `gorm:"column:apply_type"`  // 申请类型:10-充值,20-支付订单,30-订单退款,40-手工增加,41-手工减少
	Amount     int       `gorm:"column:amount_fen"`
	Remark     string    `gorm:"column:remark"`
	Status     uint      `gorm:"column:status"`     // 交易状态:1-待处理,2-处理中,3-已撤销,4-已入账
	IsDeleted  uint      `gorm:"column:is_deleted"` // 是否已删除:0-未删除,1-已删除
	CreatedAt  time.Time `gorm:"column:create_time"`
	ModifiedAt time.Time `gorm:"column:modify_time"`
	AppID      uint      `gorm:"column:app_id"`
	Country    uint      `gorm:"column:hcountry"`
	City       uint      `gorm:"column:city_id"`
	Currency   string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m UserWalletApply) TableName() string {
	return "t_gw_user_wallet_apply_" + util.Mod(m.UserID, 32)
}
