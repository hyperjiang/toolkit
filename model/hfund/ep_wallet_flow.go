package hfund

import "time"

// EpWalletFlow - 企业钱包交易申请
type EpWalletFlow struct {
	ID              uint
	CorporateID     uint      `gorm:"column:ep_id"`
	ApplyID         uint      `gorm:"column:apply_id"`  // =t_gw_ep_wallet_apply.id
	FlowType        uint      `gorm:"column:flow_type"` // 交易类型
	OrderDisplayID  string    `gorm:"column:b_order_id"`
	BeforeBalance   uint      `gorm:"column:before_balance_fen"`
	Amount          int       `gorm:"column:amount_fen"`
	AfterBalance    uint      `gorm:"column:after_balance_fen"`
	BeforePrincipal uint      `gorm:"column:before_principal_fen"`
	PrincipalChange int       `gorm:"column:principal_change_fen"`
	AfterPrincipal  uint      `gorm:"column:after_principal_fen"`
	BeforePresent   uint      `gorm:"column:before_present_fen"`
	PresentChange   int       `gorm:"column:present_change_fen"`
	AfterPresent    uint      `gorm:"column:after_present_fen"`
	Remark          string    `gorm:"column:remark"`
	CreatedAt       time.Time `gorm:"column:create_time"`
	ModifiedAt      time.Time `gorm:"column:modify_ts"`
	Country         uint      `gorm:"column:hcountry"`
	City            uint      `gorm:"column:city_id"`
	Currency        string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (EpWalletFlow) TableName() string {
	return "t_gw_ep_wallet_flow"
}
