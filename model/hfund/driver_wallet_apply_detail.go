package hfund

import (
	"time"

	"gitlab.com/hyperjiang/toolkit/util"
)

// DriverWalletApplyDetail - 司机钱包交易明细表
type DriverWalletApplyDetail struct {
	ID              uint
	DriverID        string    `gorm:"column:driver_id"`
	ApplyNO         string    `gorm:"column:b_apply_no"`        // 业务申请单号,若无需上传则为钱包交易单号
	DwalletNO       string    `gorm:"column:p_dwallet_no"`      // 钱包交易单号
	TradeType       uint      `gorm:"column:trade_type"`        // 交易类型:100-提现,200-订单完成,300-车贴广告费,400-购买会员费,500-保证金退还,600-手工增加,700-手工减少,800-车贴罚款索赔,900-虚报完成扣除,1000-活动奖励
	TradeDetailType uint      `gorm:"column:trade_detail_type"` // 交易明细类型:101-提现,201-前置支付,202-后置支付,203-优惠码,204-优惠券,205-拉拉券,206-客服补贴,301-车贴广告费,401-购买会员,501-保证金退还,601-手工增加,701-手工减少,801-车贴罚款索赔,901-微信,902-支付宝,903-用户钱包,904-企业钱包,905-拉拉券,906-客服补贴,1001-活动奖励
	Amount          uint      `gorm:"column:amount_fen"`
	Status          uint      `gorm:"column:status"`    // 交易状态:1-待处理,2-处理中,3-已撤销,4-已入账
	BillTime        time.Time `gorm:"column:bill_time"` // 撤销或入账时间
	CreatedAt       time.Time `gorm:"column:create_time"`
	ModifiedAt      time.Time `gorm:"column:modify_time"`
	Country         uint      `gorm:"column:hcountry"`
	City            uint      `gorm:"column:city_id"`
	Currency        string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (m DriverWalletApplyDetail) TableName() string {
	return "t_gw_driver_wallet_apply_detail_" + util.Mod(m.DriverID, 32)
}
