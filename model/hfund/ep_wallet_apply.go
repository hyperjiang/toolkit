package hfund

import "time"

// EpWalletApply - 企业钱包交易申请
type EpWalletApply struct {
	ID          uint
	CorporateID string    `gorm:"column:ep_id"`
	WalletNO    string    `gorm:"column:p_ep_wallet_no"` // 钱包帐户流水号
	ApplyType   uint      `gorm:"column:apply_type"`     // 申请类型: 1冻结, 2下单结算, 3下单取消, 4线上充值, 5线下打款充值, 6财务修改增加, 7财务扣除
	Amount      uint      `gorm:"column:amount_fen"`
	ApplyNO     string    `gorm:"column:b_apply_no"`
	TradeNO     string    `gorm:"column:c_trade_no"` // 第三方交易号
	Remark      string    `gorm:"column:remark"`
	Status      uint      `gorm:"column:status"` // 1待处理, 2处理中, 3已撤销, 4已入账
	CreatedAt   time.Time `gorm:"column:create_time"`
	ModifiedAt  time.Time `gorm:"column:modify_ts"`
	Country     uint      `gorm:"column:hcountry"`
	City        uint      `gorm:"column:city_id"`
	Currency    string    `gorm:"column:hcurrency"`
}

// TableName overrides the table name
func (EpWalletApply) TableName() string {
	return "t_gw_ep_wallet_apply"
}
