package hfund

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseExtraParams(t *testing.T) {
	should := require.New(t)

	apply := new(DriverWalletApply)
	apply.ExtraParams = `{"remark":"OPS MNL April 2021\/\u8c03\u6574\u5355\u53f7:1618473860271207807","op_id":"319","op_user":"abc.def","op_ip":"112.200.210.31,10.194.77.9"}`

	params := apply.ParseExtraParams()

	should.Equal("319", params.OpID)
	should.Equal("abc.def", params.OpUser)

	r, s := params.ParseRemark()
	should.Equal("OPS MNL April 2021", r)
	should.Equal("1618473860271207807", s)
}
