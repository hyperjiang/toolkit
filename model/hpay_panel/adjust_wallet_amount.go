package hpay_panel

import "time"

const (
	ADJUST_WALLET_STATUS_PENDING uint = iota + 1 // 待处理
	ADJUST_WALLET_STATUS_OK                      // 成功
	ADJUST_WALLET_STATUS_FAILED                  // 失败
	ADJUST_WALLET_STATUS_REJECT                  // 驳回
	ADJUST_WALLET_STATUS_AUDIT                   // 审核
	ADJUST_WALLET_STATUS_RUN                     // 执行
)

// AdjustWalletAmount - 钱包余额调整
type AdjustWalletAmount struct {
	ID              uint
	ObjectID        string    `gorm:"column:object_id"`
	ObjectName      string    `gorm:"column:object_name"`
	WalletType      uint      `gorm:"column:wallet_type"`     // 1：司机钱包 2：司机保证金 3：用户钱包 4：企业钱包
	Direction       uint      `gorm:"column:direction"`       // 调整方向：1 减少，2 增加
	Amount          uint      `gorm:"column:amount_fen"`      // 金额
	Reason          uint      `gorm:"reason"`                 // 调整原因
	AuditUserID     uint      `gorm:"column:audit_user_id"`   // 审核人id
	AuditUserName   string    `gorm:"column:audit_user_name"` // 审核人姓名
	AuditedAt       time.Time `gorm:"column:audit_time"`      // 审核时间
	ApplyUserID     string    `gorm:"column:apply_user_id"`   // 申请人id
	ApplyUserName   string    `gorm:"column:apply_user_name"` // 申请人姓名
	Status          uint      `gorm:"column:status"`
	RejectiD        uint      `gorm:"column:reject_id"`        // 驳回原因id
	RejectReason    string    `gorm:"column:reject_reason"`    // 驳回原因描述
	RunUserID       uint      `gorm:"column:run_user_id"`      // 执行人id
	RunUserName     string    `gorm:"column:run_user_name"`    // 执行人名称
	OperationStatus uint      `gorm:"column:operation_status"` // 1 审核通过 2 审核驳回 3 执行通过 4 执行驳回
	RunAt           time.Time `gorm:"column:run_time"`         // 执行时间
	BatchNO         string    `gorm:"column:batch_no"`         // 操作批次号
	SerialNO        string    `gorm:"column:serial_no"`        // 序列号
	Remark          string    `gorm:"column:remark"`
	CreatedAt       time.Time `gorm:"column:create_time"`
	ModifiedAt      time.Time `gorm:"column:modify_time"`
	Country         uint      `gorm:"column:hcountry"`
	City            uint      `gorm:"column:city_id"`
	Currency        string    `gorm:"column:hcurrency"`
	Language        string    `gorm:"column:hlang"`
}

// TableName overrides the table name
func (m AdjustWalletAmount) TableName() string {
	return "t_adjust_wallet_amount"
}
