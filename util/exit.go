package util

import "github.com/rs/zerolog/log"

// Exit logs error and exit
func Exit(err error) {
	log.Fatal().Err(err).Msg("")
}
