package util

import "time"

// Timezone is the default timezone
var Timezone = "UTC"

// Location is the default location
var Location = time.UTC

// SetLocation sets default location by timezone
func SetLocation(timezone string) error {
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		return err
	}
	Timezone = timezone
	Location = loc
	return nil
}

// Now returns current time in default timezone
func Now() time.Time {
	return time.Now().In(Location)
}

// NowString returns current time string in yyyy-mm-dd hh:mm:ss format
func NowString() string {
	return Format(Now())
}

// Format converts time into yyyy-mm-dd hh:mm:ss format string in default timezone
func Format(t time.Time) string {
	return t.In(Location).Format("2006-01-02 15:04:05")
}

// FormatDate converts time into yyyy-mm-dd format string in default timezone
func FormatDate(t time.Time) string {
	return t.In(Location).Format("2006-01-02")
}

// Today returns current day
func Today() time.Time {
	return Midnight(Now())
}

// Tomorrow returns the day of tomorrow
func Tomorrow() time.Time {
	return Midnight(Now().AddDate(0, 0, 1))
}

// Yesterday returns the day of yesterday
func Yesterday() time.Time {
	return Midnight(Now().AddDate(0, 0, -1))
}

// Midnight returns the midnight of t
func Midnight(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, Location)
}

// EndOfDay returns the end of a day
func EndOfDay(t time.Time) time.Time {
	return Midnight(t).AddDate(0, 0, 1).Add(-1 * time.Nanosecond)
}
