package util

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMod(t *testing.T) {
	should := require.New(t)

	should.Equal("00", Mod("65696", 32))
	should.Equal("087", Mod("236119", 256))
	should.Equal("19", Mod("25587", 32))
	should.Equal("034", Mod("469026", 256))
	should.Equal("230", Mod("469478", 256))
}

func TestCircle(t *testing.T) {
	should := require.New(t)

	should.Equal("128", Circle("10019121910585717510020048641760", 1024, 16))
	should.Equal("160", Circle("10019121910585717510020048641760", 1024, 32))

	should.Equal("320", Circle("20201105000406510235", 1024, 32))
}

func TestYearMonth(t *testing.T) {
	should := require.New(t)

	v := time.Date(2020, 11, 1, 0, 0, 0, 0, time.UTC)

	should.Equal("202011", YearMonth(v))
}
