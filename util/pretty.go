package util

import (
	"encoding/json"
	"fmt"
)

// PrintPrettyJSON -
func PrintPrettyJSON(v interface{}) {
	b, err := json.MarshalIndent(v, "", "    ")
	if err != nil {
		fmt.Println("Invalid json!")
	}

	fmt.Printf("%s\n", b)
}
