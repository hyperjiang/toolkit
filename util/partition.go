package util

import (
	"fmt"
	"hash/crc32"
	"math"
	"strconv"
	"time"
)

// Mod -
func Mod(v string, num int) string {
	f, _ := strconv.ParseFloat(v, 64)

	m := math.Mod(f, float64(num))

	n := int(math.Floor(math.Log10(float64(num)))) + 1

	return fmt.Sprintf("%0"+strconv.Itoa(n)+"d", int(m))
}

// Circle -
func Circle(v string, size, num int) string {

	sep := uint32(math.Floor(float64(size) / float64(num)))

	mod := crc32.ChecksumIEEE([]byte(v)) % uint32(size)

	var i, s uint32
	for {
		s = i * sep
		if mod < s+sep {
			break
		}
		i++
	}

	return fmt.Sprintf("%d", s)
}

// YearMonth -
func YearMonth(v time.Time) string {
	return v.Format("200601")
}
