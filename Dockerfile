FROM golang:1.15-alpine as builder

ENV GOPROXY=https://goproxy.cn,direct

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -o /tmp/main .

FROM gcr.io/distroless/base

COPY --from=builder /tmp/main /toolkit

ENTRYPOINT ["/toolkit"]
