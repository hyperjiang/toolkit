package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	hm "gitlab.com/hyperjiang/toolkit/model/hpay"
	"gitlab.com/hyperjiang/toolkit/rpc/hpay"
	"gitlab.com/hyperjiang/toolkit/util"
	"gorm.io/gorm"
)

var syncPayCmd = &cobra.Command{
	Use:   "sync-pay",
	Short: "Sync payment status",
	Run: func(cmd *cobra.Command, args []string) {

		var payRelations []hm.PayRelation

		res := global.DB("hpay").Debug().Table("t_gw_pay_relation_202105").
			Where("pay_channel_id = 1100 AND order_status = 1 AND create_time BETWEEN '2021-05-13 20:00:00' AND '2021-05-14 12:20:00'").
			Find(&payRelations)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		}

		var count int
		for _, pr := range payRelations {
			req := hpay.SyncPayRequest{
				Country: pr.Country,
				City:    pr.City,
				AppID:   pr.AppID,
				OrderNO: pr.OrderNO,
			}

			if res, err := hpay.SyncPay(req); err == nil {
				fmt.Println(res)
				count++
			}
		}

		fmt.Printf("total success: %d\n", count)
	},
}
