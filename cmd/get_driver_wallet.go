package cmd

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/rpc/hpay"
)

var getDriverWalletCmd = &cobra.Command{
	Use:   "get-driver-wallet country city driver_id",
	Short: "Get driver wallet",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 3 {
			log.Fatal().Msg("please input country, city and driver id")
		}

		req := hpay.GetDriverWalletRequest{
			Country:  args[0],
			City:     args[1],
			DriverID: args[2],
		}

		if res, err := hpay.GetDriverWallet(req); err == nil {
			fmt.Println(res)
		}
	},
}
