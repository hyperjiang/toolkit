package cmd

import (
	"errors"
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/driver"
	"gitlab.com/hyperjiang/toolkit/model/hfund"
	"gitlab.com/hyperjiang/toolkit/model/hpay_panel"
	"gitlab.com/hyperjiang/toolkit/util"
	"gorm.io/gorm"
)

var checkAdjustmentCmd = &cobra.Command{
	Use:   "check-adjustment",
	Short: "Check adjustment",
	Run: func(cmd *cobra.Command, args []string) {

		date := time.Now().Format("2006-01-02")
		if len(args) > 0 {
			date = args[0]
		}

		var count int
		for i := 0; i < 32; i++ {
			tb := fmt.Sprintf("t_gw_driver_wallet_apply_%02d", i)

			var applies []hfund.DriverWalletApply

			res := global.DB("hfund").Table(tb).
				Where("extra_params!='' AND status=4 AND trade_type=700 AND create_time BETWEEN ? AND ?", date, date+" 23:59:59").
				Order("id ASC").
				Limit(2000).
				Find(&applies)
			if res.Error != nil {
				if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
					util.Exit(res.Error)
				}
			}

			for _, apply := range applies {
				// util.PrintPrettyJSON(apply)

				params := apply.ParseExtraParams()
				remark, serialNO := params.ParseRemark()

				if len(serialNO) < 14 {
					continue
				}

				var awa hpay_panel.AdjustWalletAmount
				res := global.DB("hpay_panel").Table("t_adjust_wallet_amount").
					Where("serial_no=?", serialNO).
					First(&awa)
				if res.Error != nil {
					if errors.Is(res.Error, gorm.ErrRecordNotFound) {
						count++

						// insert back
						awa.Amount = apply.Amount
						awa.ApplyUserID = params.OpID
						awa.ApplyUserName = params.OpUser
						awa.BatchNO = serialNO[0:14]
						awa.City = apply.City
						awa.Country = apply.Country
						awa.CreatedAt = apply.CreatedAt
						awa.Currency = apply.Currency
						awa.Direction = 1
						awa.ModifiedAt = apply.ModifiedAt
						awa.ObjectID = apply.DriverID
						awa.ObjectName = getDriverName(apply.DriverID)
						awa.Reason = 11011
						awa.Remark = remark
						awa.SerialNO = serialNO
						awa.Status = 2
						awa.WalletType = 1

						r := global.DB("hpay_panel").Table("t_adjust_wallet_amount").Create(&awa)
						if r.Error != nil {
							util.Exit(r.Error)
						}

					} else {
						util.Exit(res.Error)
					}
				}
			}
		}
		fmt.Printf("total missing records: %d\n", count)
	},
}

func getDriverName(driverID string) string {
	var d driver.Driver
	res := global.DB("driver").Where("driver_id = ?", driverID).First(&d)
	if res.Error == nil {
		return d.Name
	}

	return ""
}
