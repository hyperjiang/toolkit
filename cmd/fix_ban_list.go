package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/user"
	"gitlab.com/hyperjiang/toolkit/util"
)

// deprecated
var fixBanListCmd = &cobra.Command{
	Use:   "fix-ban-list",
	Short: "Fix ban list",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			return
		}

		result := global.DB("user").Debug().Model(user.Fleet{}).
			Where("ep_id = ? AND is_favorite = 1 AND txModify='2020-11-02 21:06:53'", args[0]).
			Updates(map[string]interface{}{
				"is_ban":      1,
				"is_favorite": 0,
			})

		if result.Error != nil {
			util.Exit(result.Error)
		}

		fmt.Printf("%d rows updated\n", result.RowsAffected)
	},
}
