package cmd

import (
	"fmt"

	"github.com/hyperjiang/php"
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/util"
)

var getOrderPaymentStatsCmd = &cobra.Command{
	Use:   "get-order-payment-stats [start date] [end date]",
	Short: "Get order payment stats",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 || len(args) > 2 {
			return
		}

		start := args[0]
		end := php.Today("Y-m-d 23:59:59")

		if len(args) == 2 {
			end = args[1] + " 23:59:59"
		}

		var paidAmount, refundedAmount float64
		for i := 0; i < 256; i++ {
			var amount float64
			table := "t_order_bill_" + fmt.Sprintf("%03d", i)

			res := global.DB("bill").Table(table).
				Select("IFNULL(SUM(paid_price_fen), 0) as amount").
				Where("modify_time BETWEEN ? AND ?", start, end).
				Where("pay_status = 102 AND pay_type != 0").
				Scan(&amount)
			if res.Error != nil {
				util.Exit(res.Error)
			}

			paidAmount += amount

			res = global.DB("bill").Table(table).
				Select("IFNULL(SUM(paid_price_fen), 0) as amount").
				Where("modify_time BETWEEN ? AND ?", start, end).
				Where("pay_status = 202 AND pay_type != 0").
				Scan(&amount)
			if res.Error != nil {
				util.Exit(res.Error)
			}

			refundedAmount += amount
		}

		var userSpent float64
		for i := 0; i < 256; i++ {
			var amount float64
			table := "t_quote_" + fmt.Sprintf("%03d", i)

			res := global.DB("bill").Table(table).
				Select("IFNULL(SUM(paid_amount_fen), 0) as amount").
				Where("modify_time BETWEEN ? AND ?", start, end).
				Where("trans_status = 2 AND city_id = 91001").
				Scan(&amount)
			if res.Error != nil {
				util.Exit(res.Error)
			}

			userSpent += amount
		}

		var refundedAmount2 float64
		res := global.DB("bill").Table("t_refund_flow").
			Select("IFNULL(SUM(refund_amount_fen), 0) as amount").
			Where("modify_time BETWEEN ? AND ?", start, end).
			Where("refund_status = 3 AND city_id = 91001 AND payment_type != 0").
			Scan(&refundedAmount2)
		if res.Error != nil {
			util.Exit(res.Error)
		}

		var driverIncome float64
		for i := 0; i < 256; i++ {
			var amount float64
			table := "t_dwallet_entry_" + fmt.Sprintf("%03d", i)

			res := global.DB("bill").Table(table).
				Select("IFNULL(SUM(entry_amount_fen), 0) as amount").
				Where("modify_time BETWEEN ? AND ?", start, end).
				Where("entry_status = 3 AND city_id = 91001").
				Scan(&amount)
			if res.Error != nil {
				util.Exit(res.Error)
			}

			driverIncome += amount
		}

		fmt.Printf("order paid amount: %s\n", php.DefaultNumberFormat(paidAmount/100, 0))
		fmt.Printf("order refunded amount: %s\n", php.DefaultNumberFormat(refundedAmount/100, 0))
		fmt.Printf("HK user spent: %s\n", php.DefaultNumberFormat(userSpent/100, 0))
		fmt.Printf("HK refunded amount: %s\n", php.DefaultNumberFormat(refundedAmount2/100, 0))
		fmt.Printf("HK driver income: %s\n", php.DefaultNumberFormat(driverIncome/100, 0))
	},
}
