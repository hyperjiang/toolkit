package cmd

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/util"
)

var rootCmd = &cobra.Command{
	Use:   "toolkit",
	Short: "My Swiss Army Knife",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

// Execute -
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		util.Exit(err)
	}
}

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	cobra.OnInitialize(initConfig, initGlobal)

	rootCmd.AddCommand(checkAdjustmentCmd)
	// rootCmd.AddCommand(fixBanListCmd)
	rootCmd.AddCommand(executeRefundCmd)
	rootCmd.AddCommand(getDriverWalletCmd)
	rootCmd.AddCommand(getOrderPaymentStatsCmd)
	rootCmd.AddCommand(getPaymentRecordsCmd)
	rootCmd.AddCommand(getRefundRecordsCmd)
	rootCmd.AddCommand(getTopupFailedRecordsCmd)
	rootCmd.AddCommand(removeDuplicateFleetCmd)
	rootCmd.AddCommand(syncPayCmd)
	rootCmd.AddCommand(versionCmd)
}

func initConfig() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.AddConfigPath("/")
	viper.AddConfigPath("$HOME")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		log.Debug().Msgf("Using config file: %s", viper.ConfigFileUsed())
	} else {
		util.Exit(err)
	}
}

func initGlobal() {
	for k, v := range viper.GetStringMapString("mysql") {
		if err := global.OpenMySQL(k, v); err != nil {
			util.Exit(err)
		}
	}
}
