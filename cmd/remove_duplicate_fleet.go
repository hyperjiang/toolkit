package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/user"
)

var removeDuplicateFleetCmd = &cobra.Command{
	Use:   "remove-duplicate-fleet",
	Short: "Remove duplicate fleet",
	Run: func(cmd *cobra.Command, args []string) {
		type Result struct {
			ID    uint
			Count uint `gorm:"column:c"`
		}

		var users []Result

		if len(args) > 0 && args[0] == "1" {
			// delete duplicate records for corporates
			global.DB("user").Debug().Model(&user.Fleet{}).
				Select("ep_id AS id, COUNT(servicer_idvanDriver) AS c, COUNT(DISTINCT(servicer_idvanDriver)) AS c2").
				Where("ep_id > 0").
				Group("ep_id").
				Having("c > c2").
				Find(&users)

			for _, u := range users {
				var drivers []Result
				global.DB("user").Model(&user.Fleet{}).
					Select("servicer_idvanDriver AS id, COUNT(servicer_idvanDriver) AS c").
					Where("ep_id = ?", u.ID).
					Group("servicer_idvanDriver").
					Having("c > 1").
					Find(&drivers)

				for _, d := range drivers {
					var fleet user.Fleet

					global.DB("user").Debug().
						Where("ep_id = ? AND servicer_idvanDriver = ? AND (is_favorite = 1 OR is_ban = 1)", u.ID, d.ID).
						Order("id DESC").
						First(&fleet)

					global.DB("user").Debug().Delete(
						user.Fleet{},
						"ep_id = ? AND servicer_idvanDriver = ? AND id < ?",
						u.ID,
						d.ID,
						fleet.ID,
					)
				}
			}
		} else {
			// delete duplicate records for users
			global.DB("user").Debug().Model(&user.Fleet{}).
				Select("idvansmslogin AS id, COUNT(servicer_idvanDriver) AS c, COUNT(DISTINCT(servicer_idvanDriver)) AS c2").
				Where("idvansmslogin > 0").
				Group("idvansmslogin").
				Having("c > c2").
				Find(&users)

			for _, u := range users {
				var drivers []Result
				global.DB("user").Model(&user.Fleet{}).
					Select("servicer_idvanDriver AS id, COUNT(servicer_idvanDriver) AS c").
					Where("idvansmslogin = ?", u.ID).
					Group("servicer_idvanDriver").
					Having("c > 1").
					Find(&drivers)

				for _, d := range drivers {
					var fleet user.Fleet

					global.DB("user").Debug().
						Where("idvansmslogin = ? AND servicer_idvanDriver = ? AND (is_favorite = 1 OR is_ban = 1)", u.ID, d.ID).
						Order("id DESC").
						First(&fleet)

					global.DB("user").Debug().Delete(
						user.Fleet{},
						"idvansmslogin = ? AND servicer_idvanDriver = ? AND id < ?",
						u.ID,
						d.ID,
						fleet.ID,
					)
				}
			}
		}
	},
}
