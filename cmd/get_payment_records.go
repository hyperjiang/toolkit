package cmd

import (
	"errors"
	"hash/crc32"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/bill"
	"gitlab.com/hyperjiang/toolkit/model/hfund"
	"gitlab.com/hyperjiang/toolkit/model/hpay"
	"gitlab.com/hyperjiang/toolkit/model/user"
	"gitlab.com/hyperjiang/toolkit/util"
	"gorm.io/gorm"
)

var getPaymentRecordsCmd = &cobra.Command{
	Use:   "get-payment-records",
	Short: "Get payment records",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			return
		}

		log.Logger.Info().Msg("==================== db_user ====================")

		var order user.Order
		res := global.DB("user").Debug().
			Where("order_id = ? OR order_display_id = ?", args[0], args[0]).
			First(&order)
		if res.Error != nil {
			util.Exit(res.Error)
		}
		util.PrintPrettyJSON(order)

		var payment user.Payment
		res = global.DB("user").Debug().Where("txUUID = ?", order.UUID).First(&payment)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(payment)
		}

		var rearPays []user.RearPay
		res = global.DB("user").Debug().Model(new(user.RearPay)).
			Where("order_id = ?", order.OrderID).
			Order("id").
			Find(&rearPays)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(rearPays)
		}

		log.Logger.Info().Msg("==================== db_bill ====================")

		var quote bill.Quote
		quote.OrderID = order.OrderID

		var quotes []bill.Quote
		res = global.DB("bill").Debug().Table(quote.TableName()).
			Where("order_id = ?", order.OrderID).
			Order("quote_id").
			Find(&quotes)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(quotes)
		}

		var orderBill bill.OrderBill
		orderBill.OrderID = order.OrderID

		var orderBills []bill.OrderBill
		res = global.DB("bill").Debug().Table(orderBill.TableName()).
			Where("order_id = ?", order.OrderID).
			Order("bill_id").
			Find(&orderBills)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(orderBills)
		}

		var dwalletEntry bill.DwalletEntry
		dwalletEntry.OrderID = order.OrderID

		var dwalletEntries []bill.DwalletEntry
		res = global.DB("bill").Debug().Table(dwalletEntry.TableName()).
			Where("order_id = ?", order.OrderID).
			Order("entry_id").
			Find(&dwalletEntries)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(dwalletEntries)
		}

		log.Logger.Info().Msg("==================== db_hpay ====================")

		for _, q := range quotes {
			orderNOcrc32 := crc32.ChecksumIEEE([]byte(q.TradeNO))

			var payApply hpay.PayApply
			payApply.OrderNO = q.TradeNO

			res = global.DB("hpay").Debug().Table(payApply.TableName()).Where("b_order_no_crc32 = ?", orderNOcrc32).First(&payApply)
			if res.Error == nil {
				util.PrintPrettyJSON(payApply)
			}

			var userPayFlow hpay.UserPayFlow
			res = global.DB("hpay").Debug().Table(userPayFlow.TableName()).Where("b_order_no_crc32 = ?", orderNOcrc32).First(&userPayFlow)
			if res.Error == nil {
				util.PrintPrettyJSON(userPayFlow)
			}

			var payFlow hpay.PayFlow
			payFlow.OrderNO = q.TradeNO

			res = global.DB("hpay").Debug().Table(payFlow.TableName()).Where("b_order_no_crc32 = ?", orderNOcrc32).First(&payFlow)
			if res.Error == nil {
				util.PrintPrettyJSON(payFlow)
			}

			var payRelation hpay.PayRelation
			payRelation.CreatedAt = q.CreatedAt

			res = global.DB("hpay").Debug().Table(payRelation.TableName()).Where("b_order_no_crc32 = ?", orderNOcrc32).First(&payRelation)
			if res.Error == nil {
				util.PrintPrettyJSON(payRelation)
			}

			var uwalletApply hfund.UserWalletApply
			uwalletApply.UserID = order.UserID

			res = global.DB("hfund").Debug().Table(uwalletApply.TableName()).
				Where("b_apply_no = ?", payRelation.PayNO).
				First(&uwalletApply)
			if res.Error != nil {
				if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
					util.Exit(res.Error)
				}
			} else {
				util.PrintPrettyJSON(uwalletApply)
			}

			var uwalletFlow hfund.UserWalletFlow
			uwalletFlow.UserID = order.UserID

			res = global.DB("hfund").Debug().Table(uwalletFlow.TableName()).
				Where("p_wallet_no_crc32 = ?", crc32.ChecksumIEEE([]byte(uwalletApply.WalletNO))).
				First(&uwalletFlow)
			if res.Error != nil {
				if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
					util.Exit(res.Error)
				}
			} else {
				util.PrintPrettyJSON(uwalletFlow)
			}

			if order.CorporateID > 0 {
				var epWalletApply hfund.EpWalletApply

				res = global.DB("hfund").Debug().Model(&epWalletApply).
					Where("p_ep_wallet_no = ? AND b_apply_no = ?", payRelation.TradeNO, payRelation.PayNO).
					First(&epWalletApply)
				if res.Error == nil {
					util.PrintPrettyJSON(epWalletApply)

					var epWalletFlow hfund.EpWalletFlow
					res = global.DB("hfund").Debug().Model(&epWalletFlow).
						Where("apply_id = ? AND b_order_id = ?", epWalletApply.ID, order.DisplayID).
						First(&epWalletFlow)
					if res.Error == nil {
						util.PrintPrettyJSON(epWalletFlow)
					}
				}
			}
		}

		log.Logger.Info().Msg("==================== db_hfund ====================")
		var dwalletApply hfund.DriverWalletApply
		dwalletApply.DriverID = order.DriverID

		var dwalletApplies []hfund.DriverWalletApply
		res = global.DB("hfund").Debug().Table(dwalletApply.TableName()).
			Where("b_apply_no = ?", order.DisplayID).
			Order("id").
			Find(&dwalletApplies)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(dwalletApplies)
		}

		var dwalletApplyDetail hfund.DriverWalletApplyDetail
		dwalletApplyDetail.DriverID = order.DriverID

		var dwalletApplyDetails []hfund.DriverWalletApplyDetail
		res = global.DB("hfund").Debug().Table(dwalletApplyDetail.TableName()).
			Where("b_apply_no = ?", order.DisplayID).
			Order("id").
			Find(&dwalletApplyDetails)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(dwalletApplyDetails)
		}

		var dwalletFlow hfund.DriverWalletFlow
		dwalletFlow.DriverID = order.DriverID

		var dwalletFlows []hfund.DriverWalletFlow
		res = global.DB("hfund").Debug().Table(dwalletFlow.TableName()).
			Where("b_apply_no = ?", order.DisplayID).
			Order("id").
			Find(&dwalletFlows)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(dwalletFlows)
		}
	},
}
