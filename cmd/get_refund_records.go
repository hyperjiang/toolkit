package cmd

import (
	"errors"
	"hash/crc32"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/bill"
	"gitlab.com/hyperjiang/toolkit/model/hpay"
	"gitlab.com/hyperjiang/toolkit/model/user"
	"gitlab.com/hyperjiang/toolkit/util"
	"gorm.io/gorm"
)

var getRefundRecordsCmd = &cobra.Command{
	Use:   "get-refund-records",
	Short: "Get refund records",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			return
		}

		log.Logger.Info().Msg("==================== db_user ====================")

		var order user.Order
		res := global.DB("user").Debug().
			Where("order_id = ? OR order_display_id = ? OR order_uuid = ?", args[0], args[0], args[0]).
			First(&order)
		if res.Error != nil {
			util.Exit(res.Error)
		}
		util.PrintPrettyJSON(order)

		log.Logger.Info().Msg("==================== db_bill ====================")

		var refunds []bill.RefundFlow
		res = global.DB("bill").Debug().Model(new(bill.RefundFlow)).
			Where("order_id = ?", order.OrderID).
			Order("refund_id").
			Find(&refunds)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		} else {
			util.PrintPrettyJSON(refunds)
		}

		log.Logger.Info().Msg("==================== db_hpay ====================")

		for _, q := range refunds {
			var refundApply hpay.RefundApply
			res = global.DB("hpay").Debug().
				Where("b_order_no_crc32 = ?", crc32.ChecksumIEEE([]byte(q.TradeNO))).
				First(&refundApply)
			if res.Error == nil {
				util.PrintPrettyJSON(refundApply)
			}

			var refundFlow hpay.RefundFlow
			res = global.DB("hpay").Debug().
				Where("p_refund_no_crc32 = ?", crc32.ChecksumIEEE([]byte(refundApply.PRefundNO))).
				First(&refundFlow)
			if res.Error == nil {
				util.PrintPrettyJSON(refundFlow)
			}
		}
	},
}
