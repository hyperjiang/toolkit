package cmd

import (
	"fmt"
	"hash/crc32"

	excelize "github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/hyperjiang/php"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	"gitlab.com/hyperjiang/toolkit/model/driver"
	"gitlab.com/hyperjiang/toolkit/model/ep"
	"gitlab.com/hyperjiang/toolkit/model/hfund"
	"gitlab.com/hyperjiang/toolkit/model/hpay"
	"gitlab.com/hyperjiang/toolkit/model/user"
	"gitlab.com/hyperjiang/toolkit/util"
)

// 10002 - 司机会员费
// 10003 - 司机保证金
// 10007 - 企业钱包充值
// 10008 - 用户钱包充值
// 10032 - 司机充值
var getTopupFailedRecordsCmd = &cobra.Command{
	Use:   "get-topup-failed-records [app_id] [country]",
	Short: "Get topup failed records",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			return
		}

		appID := args[0]
		country := args[1]

		if !php.InArray(appID, []string{"10003", "10007", "10008", "10032"}) {
			log.Logger.Error().Msgf("Unsupported app id: %s", appID)
			return
		}

		if country == "" {
			log.Logger.Error().Msgf("Please input country code")
			return
		}

		var payRelations []hpay.PayRelation

		res := global.DB("hpay").Table("t_gw_pay_relation_202102").
			Where("order_status = 2 AND app_id = ? AND hcountry = ? AND create_time >= '2021-02-06 02:00:00' AND create_time <= '2021-02-06 10:10:00'", appID, country).
			Find(&payRelations)
		if res.Error != nil {
			util.Exit(res.Error)
		}

		switch country {
		case "30000":
			country = "SG"
		case "40000":
			country = "MY"
		case "50000":
			country = "PH"
		case "80000":
			country = "TW"
		case "90000":
			country = "HK"
		}

		var epWalletApplies []hfund.EpWalletApply
		var uwalletApplies []hfund.UserWalletApply
		var dwalletApplies []hfund.DriverWalletApply

		var count int
		for _, pr := range payRelations {
			switch appID {
			case "10003":
				// 司机保证金
			case "10007":
				// 企业充值
				var epWalletApply hfund.EpWalletApply
				res = global.DB("hfund").Table(epWalletApply.TableName()).
					Where("status != 4 AND p_ep_wallet_no_crc32 = ? AND ep_id = ?", crc32.ChecksumIEEE([]byte(pr.OrderNO)), pr.UID).
					First(&epWalletApply)
				if res.Error != nil {
					// log.Logger.Info().Msgf("No unsuccessful apply record for: %s, %s", pr.OrderNO, pr.UID)
					continue
				}
				// util.PrintPrettyJSON(epWalletApply)
				epWalletApplies = append(epWalletApplies, epWalletApply)
				count++

			case "10008":
				// 用户充值
				var uwalletApply hfund.UserWalletApply
				uwalletApply.UserID = pr.UID

				res = global.DB("hfund").Table(uwalletApply.TableName()).
					Where("status != 4 AND p_wallet_no = ?", pr.OrderNO).
					First(&uwalletApply)
				if res.Error != nil {
					// log.Logger.Info().Msgf("No unsuccessful apply record for: %s, %s", pr.OrderNO, pr.UID)
					continue
				}
				// util.PrintPrettyJSON(uwalletApply)
				uwalletApplies = append(uwalletApplies, uwalletApply)
				count++

			case "10032":
				// 司机充值
				var dwalletApply hfund.DriverWalletApply
				dwalletApply.DriverID = pr.UID

				res = global.DB("hfund").Table(dwalletApply.TableName()).
					Where("status != 4 AND b_apply_no = ?", pr.OrderNO).
					First(&dwalletApply)
				if res.Error != nil {
					// log.Logger.Info().Msgf("No unsuccessful apply record for: %s, %s", pr.OrderNO, pr.UID)
					continue
				}
				// util.PrintPrettyJSON(dwalletApply)
				dwalletApplies = append(dwalletApplies, dwalletApply)
				count++
			}
		}

		fmt.Printf("total affected records: %d", count)

		switch appID {
		case "10003":
			// 司机保证金
		case "10007":
			// 企业充值
			generateCorporateTopup(country, epWalletApplies)
			generateCorporateTopupImport(country, epWalletApplies)
		case "10008":
			// 用户充值
			generateUserTopup(country, uwalletApplies)
			generateUserTopupImport(country, uwalletApplies)
		case "10032":
			// 司机充值
			generateDriverTopup(country, dwalletApplies)
			generateDriverTopupImport(country, dwalletApplies)
		}
	},
}

func generateCorporateTopup(country string, epWalletApplies []hfund.EpWalletApply) {
	// 企业充值
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "Corporate ID")
	f.SetCellValue("Sheet1", "B1", "Name")
	f.SetCellValue("Sheet1", "C1", "Phone")
	f.SetCellValue("Sheet1", "D1", "p_ep_wallet_no")
	f.SetCellValue("Sheet1", "E1", "b_apply_no")
	f.SetCellValue("Sheet1", "F1", "Amount")
	f.SetCellValue("Sheet1", "G1", "Currency")
	f.SetCellValue("Sheet1", "H1", "Create Time")

	i := 2
	for _, r := range epWalletApplies {
		var e ep.Ep
		var name, phone string
		res := global.DB("ep").Table(e.TableName()).
			Where("ep_id = ?", r.CorporateID).
			First(&e)
		if res.Error == nil {
			name = e.Name
		}

		var staff ep.Staff
		res = global.DB("ep").Table(staff.TableName()).
			Where("ep_id = ?", r.CorporateID).
			First(&staff)
		if res.Error == nil {
			phone = staff.Phone
		}

		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.CorporateID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), name)
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), phone)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), r.WalletNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), r.ApplyNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("F%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("G%d", i), r.Currency)
		f.SetCellValue("Sheet1", fmt.Sprintf("H%d", i), r.CreatedAt.Format("2006-01-02 15:04:05"))
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_ep_topup.xlsx"); err != nil {
		util.Exit(err)
	}
}

func generateCorporateTopupImport(country string, epWalletApplies []hfund.EpWalletApply) {
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "enterpriseNumbering")
	f.SetCellValue("Sheet1", "B1", "Adjust direction")
	f.SetCellValue("Sheet1", "C1", "Adjustment amount")
	f.SetCellValue("Sheet1", "D1", "Reason for adjustment")
	f.SetCellValue("Sheet1", "E1", "Note")

	i := 2
	for _, r := range epWalletApplies {
		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.CorporateID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), "Increase balance")
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), "Compensation")
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), "Compensation for "+r.ApplyNO)
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_ep_topup_import.xlsx"); err != nil {
		util.Exit(err)
	}
}

func generateUserTopup(country string, uwalletApplies []hfund.UserWalletApply) {
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "User ID")
	f.SetCellValue("Sheet1", "B1", "Name")
	f.SetCellValue("Sheet1", "C1", "Phone")
	f.SetCellValue("Sheet1", "D1", "p_wallet_no")
	f.SetCellValue("Sheet1", "E1", "b_apply_no")
	f.SetCellValue("Sheet1", "F1", "Amount")
	f.SetCellValue("Sheet1", "G1", "Currency")
	f.SetCellValue("Sheet1", "H1", "Create Time")

	i := 2
	for _, r := range uwalletApplies {
		var login user.Vansmslogin
		var name, phone string
		res := global.DB("user").Table(login.TableName()).
			Where("user_id = ?", r.UserID).
			First(&login)
		if res.Error == nil {
			name = login.NickName
			phone = login.Phone
		}

		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.UserID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), name)
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), phone)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), r.WalletNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), r.ApplyNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("F%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("G%d", i), r.Currency)
		f.SetCellValue("Sheet1", fmt.Sprintf("H%d", i), r.CreatedAt.Format("2006-01-02 15:04:05"))
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_user_topup.xlsx"); err != nil {
		util.Exit(err)
	}
}

func generateUserTopupImport(country string, uwalletApplies []hfund.UserWalletApply) {
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "userNumbering")
	f.SetCellValue("Sheet1", "B1", "Adjust direction")
	f.SetCellValue("Sheet1", "C1", "Adjustment amount")
	f.SetCellValue("Sheet1", "D1", "Reason for adjustment")
	f.SetCellValue("Sheet1", "E1", "Note")

	i := 2
	for _, r := range uwalletApplies {
		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.UserID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), "Increase balance")
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), "Compensation")
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), "Compensation for "+r.ApplyNO)
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_user_topup_import.xlsx"); err != nil {
		util.Exit(err)
	}
}

func generateDriverTopup(country string, dwalletApplies []hfund.DriverWalletApply) {
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "Driver ID")
	f.SetCellValue("Sheet1", "B1", "Name")
	f.SetCellValue("Sheet1", "C1", "Phone")
	f.SetCellValue("Sheet1", "D1", "p_wallet_no")
	f.SetCellValue("Sheet1", "E1", "b_apply_no")
	f.SetCellValue("Sheet1", "F1", "Amount")
	f.SetCellValue("Sheet1", "G1", "Currency")
	f.SetCellValue("Sheet1", "H1", "Create Time")

	i := 2
	for _, r := range dwalletApplies {
		var d driver.Driver
		var name, phone string
		res := global.DB("driver").Table(d.TableName()).
			Where("driver_id = ?", r.DriverID).
			First(&d)
		if res.Error == nil {
			name = d.Name
			phone = d.Phone
		}

		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.DriverID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), name)
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), phone)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), r.DwalletNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), r.ApplyNO)
		f.SetCellValue("Sheet1", fmt.Sprintf("F%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("G%d", i), r.Currency)
		f.SetCellValue("Sheet1", fmt.Sprintf("H%d", i), r.CreatedAt.Format("2006-01-02 15:04:05"))
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_driver_topup.xlsx"); err != nil {
		util.Exit(err)
	}
}

func generateDriverTopupImport(country string, dwalletApplies []hfund.DriverWalletApply) {
	f := excelize.NewFile()
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "driverNumbering")
	f.SetCellValue("Sheet1", "B1", "Adjust direction")
	f.SetCellValue("Sheet1", "C1", "Adjustment amount")
	f.SetCellValue("Sheet1", "D1", "Reason for adjustment")
	f.SetCellValue("Sheet1", "E1", "Note")

	i := 2
	for _, r := range dwalletApplies {
		f.SetCellValue("Sheet1", fmt.Sprintf("A%d", i), r.DriverID)
		f.SetCellValue("Sheet1", fmt.Sprintf("B%d", i), "Increase balance")
		f.SetCellValue("Sheet1", fmt.Sprintf("C%d", i), r.Amount/100)
		f.SetCellValue("Sheet1", fmt.Sprintf("D%d", i), "System adjustment")
		f.SetCellValue("Sheet1", fmt.Sprintf("E%d", i), "Compensation for "+r.ApplyNO)
		i++
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("/tmp/" + country + "_driver_topup_import.xlsx"); err != nil {
		util.Exit(err)
	}
}
