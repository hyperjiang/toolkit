package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/hyperjiang/toolkit/global"
	hm "gitlab.com/hyperjiang/toolkit/model/hpay"
	"gitlab.com/hyperjiang/toolkit/rpc/hpay"
	"gitlab.com/hyperjiang/toolkit/util"
	"gorm.io/gorm"
)

var executeRefundCmd = &cobra.Command{
	Use:   "execute-refund",
	Short: "Execute refund",
	Run: func(cmd *cobra.Command, args []string) {

		var refundApplies []hm.RefundApply

		res := global.DB("hpay").Debug().Table("t_gw_refund_apply").
			Where("p_refund_status = 2").
			Limit(100).
			Find(&refundApplies)
		if res.Error != nil {
			if !errors.Is(res.Error, gorm.ErrRecordNotFound) {
				util.Exit(res.Error)
			}
		}

		var count int
		for _, ra := range refundApplies {
			req := hpay.ExecuteRefundRequest{
				Country:  ra.Country,
				City:     ra.City,
				AppID:    ra.AppID,
				RefundNO: ra.PRefundNO,
			}

			if res, err := hpay.ExecuteRefund(req); err == nil {
				fmt.Println(res)
				count++
			}
		}

		fmt.Printf("total success: %d\n", count)
	},
}
