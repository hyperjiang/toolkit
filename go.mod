module gitlab.com/hyperjiang/toolkit

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.3.2
	github.com/go-resty/resty/v2 v2.6.0
	github.com/hyperjiang/php v0.6.1
	github.com/json-iterator/go v1.1.6
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
