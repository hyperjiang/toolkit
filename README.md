# Toolkit

## Remove deplicate fleet

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml registry.gitlab.com/hyperjiang/toolkit remove-duplicate-fleet 1
```

## Get payment records

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml registry.gitlab.com/hyperjiang/toolkit get-payment-records [order id / display id]
```

## Get refund records

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml registry.gitlab.com/hyperjiang/toolkit get-refund-records [order id / display id]
```

## Get order payment stats

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml registry.gitlab.com/hyperjiang/toolkit get-order-payment-stats [start date] [end date]
```

## Get failed topup

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml -v $PWD/tmp:/tmp registry.gitlab.com/hyperjiang/toolkit get-topup-failed-records [app id] [country]
```

## Get driver wallet

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml -v $PWD/tmp:/tmp registry.gitlab.com/hyperjiang/toolkit get-driver-wallet [country] [city] [driver id]
```

## Sync payment status

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml -v $PWD/tmp:/tmp registry.gitlab.com/hyperjiang/toolkit sync-pay
```

## Execute refund

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml -v $PWD/tmp:/tmp registry.gitlab.com/hyperjiang/toolkit execute-refund
```

## Check adjustment

```sh
docker run --rm -v $PWD/config.yaml:/config.yaml -v $PWD/tmp:/tmp registry.gitlab.com/hyperjiang/toolkit check-adjustment 2021-05-01
```
