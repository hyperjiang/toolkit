package global

import (
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var databases map[string]*gorm.DB

func init() {
	databases = make(map[string]*gorm.DB)
}

// OpenMySQL -
func OpenMySQL(name, dsn string) error {
	conn, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err == nil {
		db, _ := conn.DB()
		db.SetConnMaxLifetime(5 * time.Minute)
		databases[name] = conn
	}

	return err
}

// DB -
func DB(name string) *gorm.DB {
	return databases[name]
}
