package rpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMakeSign(t *testing.T) {
	secret := "51D97F289A923852721F74647789ABBA"

	params := make(map[string]string)
	params["aa"] = "xxx"
	assert.Equal(t, "6F0F3E086E882B91AE5DBA9ADD8AA17F", MakeSign(params, secret))

	params["bb"] = "yyy"
	assert.Equal(t, "FC60BABC310D21B49C4B1BBBABB41BEC", MakeSign(params, secret))
}
