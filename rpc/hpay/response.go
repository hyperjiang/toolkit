package hpay

// GetDriverWalletResponse -
type GetDriverWalletResponse struct {
	Ret  int32        `json:"ret"`
	Msg  string       `json:"msg"`
	Data DriverWallet `json:"data"`
}

// GetRet -
func (res GetDriverWalletResponse) GetRet() int32 {
	return res.Ret
}

// GetMsg -
func (res GetDriverWalletResponse) GetMsg() string {
	return res.Msg
}

// DriverWallet -
type DriverWallet struct {
	DriverID         int32  `json:"driver_id,string"`
	AvailableBalance int32  `json:"avl_balance_fen,string"`
	FreezedBalance   int32  `json:"frz_balance_fen,string"`
	Balance          int32  `json:"balance_fen,string"`
	AvailablePoints  int32  `json:"avl_member_card_total_points,string"`
	CreateTime       string `json:"create_time"`
	ModifyTime       string `json:"modify_time"`
}
