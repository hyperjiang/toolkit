package hpay

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/rpc"
)

// GetDriverWallet gets driver wallet info
func GetDriverWallet(req GetDriverWalletRequest) (*GetDriverWalletResponse, error) {
	client := rpc.NewClient(viper.GetString("hll.hpay_dwallet_url"))

	url := fmt.Sprintf(
		"index.php?hcountry=%s&city_id=%s&_m=dwallet_info&app_key=%s",
		req.Country,
		req.City,
		viper.GetString("hll.hpay_key"),
	)

	var res GetDriverWalletResponse
	err := client.Post(url, req.FormData(), &res)

	return &res, err
}

// SyncPay syncs payment status
func SyncPay(req SyncPayRequest) (*rpc.DefaultResponse, error) {
	client := rpc.NewClient(viper.GetString("hll.hpay_url"))

	url := fmt.Sprintf(
		"index.php?hcountry=%s&city_id=%s&_m=pay_gateway&_a=sync_c_pay&app_key=%s",
		req.Country,
		req.City,
		viper.GetString("hll.hpay_key"),
	)

	var res rpc.DefaultResponse
	err := client.Post(url, req.FormData(), &res)

	return &res, err
}

// ExecuteRefund syncs payment status
func ExecuteRefund(req ExecuteRefundRequest) (*rpc.DefaultResponse, error) {
	client := rpc.NewClient(viper.GetString("hll.hpay_url"))

	url := fmt.Sprintf(
		"index.php?hcountry=%s&city_id=%s&_m=refund_gateway&_a=execute_refund&app_key=%s",
		req.Country,
		req.City,
		viper.GetString("hll.hpay_key"),
	)

	var res rpc.DefaultResponse
	err := client.Post(url, req.FormData(), &res)

	return &res, err
}
