package hpay

import (
	"github.com/spf13/viper"
	"gitlab.com/hyperjiang/toolkit/rpc"
	"gitlab.com/hyperjiang/toolkit/util"
)

// GetDriverWalletRequest -
type GetDriverWalletRequest struct {
	Country   string `json:"hcountry"`
	City      string `json:"city_id"`
	AppKey    string `json:"app_key"`
	Signature string `json:"_sign"`
	DriverID  string `json:"driver_id"`
}

// FormData -
func (req *GetDriverWalletRequest) FormData() map[string]string {
	data := map[string]string{
		"app_key":   viper.GetString("hll.hpay_key"),
		"driver_id": req.DriverID,
	}

	data["_sign"] = rpc.MakeSign(data, viper.GetString("hll.hpay_secret"))

	return data
}

// SyncPayRequest -
type SyncPayRequest struct {
	Country   string `json:"hcountry"`
	City      string `json:"city_id"`
	AppKey    string `json:"app_key"`
	Signature string `json:"_sign"`
	AppID     string `json:"app_id"`
	OrderNO   string `json:"b_order_no"`
}

// FormData -
func (req *SyncPayRequest) FormData() map[string]string {
	data := map[string]string{
		"app_key":    viper.GetString("hll.hpay_key"),
		"app_id":     req.AppID,
		"b_order_no": req.OrderNO,
	}

	data["_sign"] = rpc.MakeSign(data, viper.GetString("hll.hpay_secret"))

	return data
}

// ExecuteRefundRequest -
type ExecuteRefundRequest struct {
	Country   string `json:"hcountry"`
	City      string `json:"city_id"`
	AppKey    string `json:"app_key"`
	Signature string `json:"_sign"`
	AppID     string `json:"app_id"`
	RefundNO  string `json:"p_refund_no"`
}

// FormData -
func (req *ExecuteRefundRequest) FormData() map[string]string {
	data := map[string]string{
		"app_key":     viper.GetString("hll.hpay_key"),
		"app_id":      req.AppID,
		"auth_id":     "0",
		"auth_name":   "守护进程",
		"auth_ip":     "127.0.0.1",
		"p_refund_no": req.RefundNO,
		"send_time":   util.NowString(),
	}

	data["_sign"] = rpc.MakeSign(data, viper.GetString("hll.hpay_secret"))

	return data
}
