package rpc

import (
	"crypto/md5"
	"fmt"
	"sort"
	"strings"
)

// MakeSign makes signature.
func MakeSign(params map[string]string, secret string) string {
	var keys []string
	for k := range params {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i int, j int) bool {
		return keys[i] < keys[j]
	})

	var str string
	for _, k := range keys {
		if params[k] != "" {
			str += k + params[k]
		}
	}
	str = secret + str + secret

	return strings.ToUpper(fmt.Sprintf("%x", md5.Sum([]byte(str))))
}
