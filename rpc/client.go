package rpc

import (
	"errors"
	"net/http"
	"time"

	resty "github.com/go-resty/resty/v2"
	jsoniter "github.com/json-iterator/go"
	"github.com/rs/zerolog/log"
)

const timeout = 2 * time.Second

// Response is common response interface
type Response interface {
	GetRet() int32
	GetMsg() string
}

// DefaultResponse is default response format
type DefaultResponse struct {
	Ret  int32       `json:"ret"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// GetRet gets ret field
func (res DefaultResponse) GetRet() int32 {
	return res.Ret
}

// GetMsg gets msg field
func (res DefaultResponse) GetMsg() string {
	return res.Msg
}

// Client is http client with business code
type Client struct {
	Cli *resty.Client
}

// NewClient returns a http client
func NewClient(baseURL string) *Client {
	cli := resty.New().
		SetHostURL(baseURL).
		SetHeader("Accept", "application/json").
		SetTimeout(timeout)

	return &Client{Cli: cli}
}

// Get is a helper function to make http get and save the result into res
func (client *Client) Get(url string, params map[string]string, res Response) error {
	log.Logger.Debug().Msgf("url: %s, request: %v", url, params)

	var resp, err = client.Cli.R().SetQueryParams(params).Get(url)
	if err != nil {
		log.Logger.Err(err).Msg("http request timeout")
		return err
	}

	log.Logger.Debug().Msgf("url: %s, response: %s", url, resp.String())

	return client.handleResponse(resp, res)
}

// Post is a helper function to make http post and save the result into res
func (client *Client) Post(url string, formdata map[string]string, res Response) error {
	log.Logger.Debug().Msgf("url: %s, request: %v", url, formdata)

	var resp, err = client.Cli.R().SetFormData(formdata).Post(url)
	if err != nil {
		log.Logger.Err(err).Msg("http request timeout")
		return err
	}

	log.Logger.Debug().Msgf("url: %s, response: %s", url, resp.String())

	return client.handleResponse(resp, res)
}

func (client *Client) handleResponse(resp *resty.Response, res Response) error {
	if resp.StatusCode() != http.StatusOK {
		log.Logger.Error().Msgf("error http status: %d", resp.StatusCode())
		return errors.New(resp.String())
	}

	if err := jsoniter.Unmarshal(resp.Body(), &res); err != nil {
		log.Logger.Error().Msgf("fail to unmarshal response: %s", resp.Body())

		var defaultRes DefaultResponse
		if err := jsoniter.Unmarshal(resp.Body(), &defaultRes); err == nil {
			return errors.New(defaultRes.GetMsg())
		}

		return errors.New(resp.String())
	}

	if res.GetRet() != 0 {
		log.Logger.Error().Msgf("ret: %d, msg: %s", res.GetRet(), res.GetMsg())
		return errors.New(res.GetMsg())
	}

	return nil
}
